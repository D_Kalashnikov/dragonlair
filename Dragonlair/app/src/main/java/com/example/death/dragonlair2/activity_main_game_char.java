package com.example.death.dragonlair2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;


public class activity_main_game_char extends AppCompatActivity {

    TextView txtName, txtRace, txtClass, txtWorldview, txtHealt, txtRun, txtAbout;
    EditText editName, editRace, editClass, editHealt, editRun, editAbout;
    ImageView imageAvatar;
    Spinner spinner2;
    CircleImageView imageAvatar2;

    String ImageDecode;
    String[] FILE;

    SharedPreferences sPref1_name, SP_avatar, sPref7_about, sPref2_race, sPref3_class, sPref4_worldview;
    final String SAVED_TEXT1 = "saved_text";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game_char);

        imageAvatar2 = findViewById(R.id.imageAvatar2);

        txtName = findViewById(R.id.txtName);
        txtRace = findViewById(R.id.txtRace);
        txtClass = findViewById(R.id.txtClass);
        txtWorldview = findViewById(R.id.txtWorldview);
        txtAbout = findViewById(R.id.txtAbout);

        spinner2 = findViewById(R.id.spinner2);

       /* ArrayAdapter<CharSequence> dadapter = ArrayAdapter.createFromResource(this, R.array.Worldview, R.layout.row);
        dadapter.setDropDownViewResource(R.layout.row2);
        spinner2.setAdapter(dadapter);*/

        editName = findViewById(R.id.editName);
        editRace = findViewById(R.id.editRace);
        editClass = findViewById(R.id.editClass);
        editAbout = findViewById(R.id.editAbout);



        Typeface roboto = Typeface.createFromAsset(getAssets(),
                "Roboto_Regular.ttf"); //use this.getAssets if you are calling from an Activity
        editName.setTypeface(roboto);
        editRace.setTypeface(roboto);
        editClass.setTypeface(roboto);

        Typeface roboto_light = Typeface.createFromAsset(getAssets(),
                "Roboto_Light.ttf"); //use this.getAssets if you are calling from an Activity
        txtName.setTypeface(roboto_light);
        txtRace.setTypeface(roboto_light);
        txtClass.setTypeface(roboto_light);
        txtWorldview.setTypeface(roboto_light);
        txtAbout.setTypeface(roboto_light);
        editAbout.setTypeface(roboto_light);

        load_param();
    }

    public void save_param(){
        sPref1_name = getSharedPreferences("MyPref1_name", MODE_PRIVATE);
        SharedPreferences.Editor ed1 = sPref1_name.edit();
        ed1.putString(SAVED_TEXT1, editName.getText().toString());
        ed1.apply();

        sPref2_race = getSharedPreferences("MyPref2_race", MODE_PRIVATE);
        SharedPreferences.Editor ed2 = sPref2_race.edit();
        ed2.putString(SAVED_TEXT1, editRace.getText().toString());
        ed2.apply();

        sPref3_class = getSharedPreferences("MyPref3_class", MODE_PRIVATE);
        SharedPreferences.Editor ed3 = sPref3_class.edit();
        ed3.putString(SAVED_TEXT1, editClass.getText().toString());
        ed3.apply();

        sPref4_worldview = getSharedPreferences("MyPref4_worldview", MODE_PRIVATE);
        SharedPreferences.Editor ed4 = sPref4_worldview.edit();
        ed4.putString(SAVED_TEXT1, Integer.toString(spinner2.getSelectedItemPosition()));
        ed4.apply();

        sPref7_about = getSharedPreferences("MyPref7_about", MODE_PRIVATE);
        SharedPreferences.Editor ed7 = sPref7_about.edit();
        ed7.putString(SAVED_TEXT1, editAbout.getText().toString());
        ed7.apply();

    }
    public void load_param(){
        //avatar
        SP_avatar = getSharedPreferences("MyPref_avatar", MODE_PRIVATE);
        String savedSP_avatar = SP_avatar.getString(SAVED_TEXT1, "");
        if(!savedSP_avatar.equals("")){
            imageAvatar2.setImageBitmap(BitmapFactory.decodeFile(savedSP_avatar));
        }

        //name person
        sPref1_name = getSharedPreferences("MyPref1_name", MODE_PRIVATE);
        String savedSP_name = sPref1_name.getString(SAVED_TEXT1, "Oh...");
        editName.setText(savedSP_name);

        //race
        sPref2_race = getSharedPreferences("MyPref2_race", MODE_PRIVATE);
        String savedSPC_race = sPref2_race.getString(SAVED_TEXT1, "Oh...");
        editRace.setText(savedSPC_race);

        //class
        sPref3_class = getSharedPreferences("MyPref3_class", MODE_PRIVATE);
        String savedSPC_class = sPref3_class.getString(SAVED_TEXT1, "Oh...");
        editClass.setText(savedSPC_class);

        //spinner load
        sPref4_worldview = getSharedPreferences("MyPref4_worldview", MODE_PRIVATE);
        String savedText4 = sPref4_worldview.getString(SAVED_TEXT1, "");
        if (savedText4.equals("0") || savedText4.equals("1") || savedText4.equals("2") || savedText4.equals("3") || savedText4.equals("4") || savedText4.equals("5") || savedText4.equals("6") || savedText4.equals("7") || savedText4.equals("8")) {
            spinner2.setSelection(Integer.valueOf(savedText4));
        }
        //about
        sPref7_about = getSharedPreferences("MyPref7_about", MODE_PRIVATE);
        String savedSP_about = sPref7_about.getString(SAVED_TEXT1, "Oh...");
        editAbout.setText(savedSP_about);
    }

    public void cls_about(View view) {
        editAbout.setText("");
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(activity_main_game_char.this, activity_main_game.class);
        save_param();
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        save_param();
    }

    @Override
    public void onPause() {
        super.onPause();
        save_param();
    }
}




