package com.example.death.dragonlair2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class activity_main_game_backpack extends AppCompatActivity {

    EditText edit_weapont_font, edit_armor_font, edit_accessory_font, edit_potions_font, edit_etc_font;

    SharedPreferences sPrefb_weapon, sPrefb_armore, sPrefb_accessor, sPrefb_potions, sPrefb_etc;
    final String SAVED_TEXT1 = "saved_text";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game_backpack);

        edit_weapont_font = findViewById(R.id.edit_weapont_font);
        edit_armor_font = findViewById(R.id.edit_armor_font);
        edit_accessory_font = findViewById(R.id.edit_accessory_font);
        edit_potions_font = findViewById(R.id.edit_potions_font);
        edit_etc_font = findViewById(R.id.edit_etc_font);

        fonts_load();

        load_backpack();

    }

    public void save_backpack(){
        sPrefb_weapon = getSharedPreferences("MyPrefb_weapon", MODE_PRIVATE);
        SharedPreferences.Editor ed_weapon = sPrefb_weapon.edit();
        ed_weapon.putString(SAVED_TEXT1, edit_weapont_font.getText().toString());
        ed_weapon.apply();

        sPrefb_armore = getSharedPreferences("MyPrefb_armore", MODE_PRIVATE);
        SharedPreferences.Editor ed_armore = sPrefb_armore.edit();
        ed_armore.putString(SAVED_TEXT1, edit_armor_font.getText().toString());
        ed_armore.apply();

        sPrefb_accessor = getSharedPreferences("MyPrefb_accessor", MODE_PRIVATE);
        SharedPreferences.Editor ed_accessor = sPrefb_accessor.edit();
        ed_accessor.putString(SAVED_TEXT1, edit_accessory_font.getText().toString());
        ed_accessor.apply();

        sPrefb_potions = getSharedPreferences("MyPrefb_potions", MODE_PRIVATE);
        SharedPreferences.Editor ed_potions = sPrefb_potions.edit();
        ed_potions.putString(SAVED_TEXT1, edit_potions_font.getText().toString());
        ed_potions.apply();

        sPrefb_etc = getSharedPreferences("MyPrefb_etc", MODE_PRIVATE);
        SharedPreferences.Editor ed_etc = sPrefb_etc.edit();
        ed_etc.putString(SAVED_TEXT1, edit_etc_font.getText().toString());
        ed_etc.apply();
    }
    public void load_backpack(){
        sPrefb_weapon = getSharedPreferences("MyPrefb_weapon", MODE_PRIVATE);
        String savedSP_weapon = sPrefb_weapon.getString(SAVED_TEXT1, "");
        edit_weapont_font.setText(savedSP_weapon);

        sPrefb_armore = getSharedPreferences("MyPrefb_armore", MODE_PRIVATE);
        String savedSP_armore = sPrefb_armore.getString(SAVED_TEXT1, "");
        edit_armor_font.setText(savedSP_armore);

        sPrefb_accessor = getSharedPreferences("MyPrefb_accessor", MODE_PRIVATE);
        String savedSP_accessor = sPrefb_accessor.getString(SAVED_TEXT1, "");
        edit_accessory_font.setText(savedSP_accessor);

        sPrefb_potions = getSharedPreferences("MyPrefb_potions", MODE_PRIVATE);
        String savedSP_potions  = sPrefb_potions.getString(SAVED_TEXT1, "");
        edit_potions_font.setText(savedSP_potions);

        sPrefb_etc = getSharedPreferences("MyPrefb_etc", MODE_PRIVATE);
        String savedSP_etc = sPrefb_etc.getString(SAVED_TEXT1, "");
        edit_etc_font.setText(savedSP_etc);
    }

    public void fonts_load(){

        Typeface roboto_light = Typeface.createFromAsset(getAssets(),
                "Roboto_Light.ttf"); //use this.getAssets if you are calling from an Activity
        edit_weapont_font.setTypeface(roboto_light);
        edit_armor_font.setTypeface(roboto_light);
        edit_accessory_font.setTypeface(roboto_light);
        edit_potions_font.setTypeface(roboto_light);
        edit_etc_font.setTypeface(roboto_light);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(activity_main_game_backpack.this, activity_main_game.class);
        save_backpack();
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        save_backpack();
    }

    @Override
    public void onPause() {
        super.onPause();
        save_backpack();
    }

    public void cls_etc(View view) {
        edit_etc_font.setText("");
    }
    public void cls_potion(View view) {
        edit_potions_font.setText("");
    }
    public void cls_acssesory(View view) {
        edit_accessory_font.setText("");
    }
    public void cls_armor(View view) {
        edit_armor_font.setText("");
    }
    public void cls_weapon(View view) {
        edit_weapont_font.setText("");
    }
}
