package com.example.death.dragonlair2;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button button, button2;
    SharedPreferences sPref1_temp;

    final String SAVED_TEXT1 = "saved_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button2.setVisibility(View.GONE);

        logic();

        Typeface roboto = Typeface.createFromAsset(getAssets(),
                "Roboto_Regular.ttf");
        button.setTypeface(roboto);
        button2.setTypeface(roboto);

    }

    public void logic(){
        String temp = "1";

        sPref1_temp = getSharedPreferences("MyPrefA_temp", MODE_PRIVATE);
        String savedSP_name = sPref1_temp.getString(SAVED_TEXT1, "0");

        if(temp.equals(savedSP_name)){
        button2.setVisibility(View.VISIBLE);
        }
    }

    public void awarning_text(View view) {

        String temp = "1";

        sPref1_temp = getSharedPreferences("MyPrefA_temp", MODE_PRIVATE);
        String savedSP_name = sPref1_temp.getString(SAVED_TEXT1, "0");

        if(temp.equals(savedSP_name)){
            custom_alert_var();
        }
        else {
            new_game();
        }
    }

    public void custom_alert_var(){

        final Dialog dialog_var = new Dialog(this);
        dialog_var.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_var.setContentView(R.layout.custom_alert_warning);
        Button dialogButton = dialog_var.findViewById(R.id.but_ok_war);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_var.dismiss();
                new_game();

            }
        });
        dialog_var.show();
        Button but_cancel_al = dialog_var.findViewById(R.id.but_cancel_war);
        but_cancel_al.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_var.dismiss();
            }
        });

    }

    public void new_game() {
        Intent intent = new Intent(MainActivity.this, activity_new.class);
        startActivity(intent);
    }
    public void btn_continue(View view) {
        Intent intent = new Intent(MainActivity.this, activity_main_game.class);
        startActivity(intent);
    }
}
