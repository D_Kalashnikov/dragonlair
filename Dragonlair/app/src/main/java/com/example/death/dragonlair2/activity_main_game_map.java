package com.example.death.dragonlair2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import uk.co.senab.photoview.PhotoViewAttacher;

public class activity_main_game_map extends AppCompatActivity {

    ImageView map;
    ImageButton loadmap;
    String ImageDecode;
    Intent intent;
    private static int IMG_RESULT = 1;

    SharedPreferences SP_map;


    final String SAVED_TEXT1 = "saved_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game_map);

        loadmap = findViewById(R.id.loadmap);
        map = findViewById(R.id.map);

        SP_map = getSharedPreferences("MyPref_map", MODE_PRIVATE);
        String savedSP_map = SP_map.getString(SAVED_TEXT1, "0");

        map.setImageBitmap(BitmapFactory.decodeFile(savedSP_map));

        PhotoViewAttacher photoView = new PhotoViewAttacher(map);
        photoView.update();

    }

    public void load_map(View view) {
        intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, IMG_RESULT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {

                Uri URI = data.getData();
                String[] FILE = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(URI, FILE, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(FILE[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();

                map.setImageBitmap(BitmapFactory.decodeFile(ImageDecode));

                //save URI
                SP_map = getSharedPreferences("MyPref_map", MODE_PRIVATE);
                SharedPreferences.Editor ed_map = SP_map.edit();
                ed_map.putString(SAVED_TEXT1, ImageDecode);
                ed_map.apply();

                PhotoViewAttacher photoView = new PhotoViewAttacher(map);
                photoView.update();

            }
        } catch (Exception e) {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG)
                    .show();
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(activity_main_game_map.this, activity_main_game.class);
        startActivity(intent);
    }


}
