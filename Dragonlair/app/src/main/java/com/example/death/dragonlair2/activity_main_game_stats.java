package com.example.death.dragonlair2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

public class activity_main_game_stats extends AppCompatActivity {

    int i = 0;

    TextView button17,button18,button19, button20, button21, editText8, editText9, editText10, editText11, editText12, editText13, editText14, editText15, editText16, editText17, editText18, editText19, editText20, editText21, editText22, editText23, editText24, editText25, editText26, editText27, editText28, editText29, editText30, editText31;
    SharedPreferences buno, buno2, buno3, buno4, buno5, buno6, buno8, buno9, buno10, buno11, buno12, buno13, buno14, buno15, buno16, buno17, buno18, buno19, buno20, buno21, buno22, buno23, buno24, buno25, buno26, buno27, buno28, buno29, buno30, buno31, bunoCheck, bunoCheck2, bunoCheck3, bunoCheck4, bunoCheck5, bunoCheck6, bunoCheck7, bunoCheck8, bunoCheck10, bunoCheck11, bunoCheck12, bunoCheck13, bunoCheck14, bunoCheck15, bunoCheck16, bunoCheck17, bunoCheck18, bunoCheck19;
    TextView button16, textView65, textView66, textView68, textView69, textView70, textView71, textView72, textView73, textView74, textView75, textView76, textView77, textView78, textView79, textView80, textView81, textView82, textView83, textView12;
    Switch checkBox, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6, checkBox7, checkBox8, checkBox10, checkBox11, checkBox12, checkBox13, checkBox14, checkBox15, checkBox16, checkBox17, checkBox18, checkBox19;
    LinearLayout linstrong, linslovk, linint, linmudr, linhar, lintel, invatlet;

    final String SAVED_TEXT1 = "saved_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game_stats);

        textView12 = findViewById(R.id.textView12);
        editText26 = findViewById(R.id.editText26);
        editText27 = findViewById(R.id.editText27);
        editText28 = findViewById(R.id.editText28);
        editText29 = findViewById(R.id.editText29);
        editText30 = findViewById(R.id.editText30);
        editText31 = findViewById(R.id.editText31);

        button16 = findViewById(R.id.button16);
        button17 = findViewById(R.id.button17);
        button18 = findViewById(R.id.button18);
        button19 = findViewById(R.id.button19);
        button20 = findViewById(R.id.button20);
        button21 = findViewById(R.id.button21);

        //Навыки силы
        textView65 = findViewById(R.id.textView65);
        editText8 = findViewById(R.id.editText8);
        checkBox = findViewById(R.id.checkBox);

        //Навыки ловкости
        textView66 = findViewById(R.id.textView66);
        editText9 = findViewById(R.id.editText9);
        textView68 = findViewById(R.id.textView68);
        editText10 = findViewById(R.id.editText10);
        textView69 = findViewById(R.id.textView69);
        editText11 = findViewById(R.id.editText11);

        checkBox2 = findViewById(R.id.checkBox2);
        checkBox3 = findViewById(R.id.checkBox3);
        checkBox4 = findViewById(R.id.checkBox4);

        //навыки интелекта
        textView70 = findViewById(R.id.textView70);
        editText12 = findViewById(R.id.editText12);
        textView71 = findViewById(R.id.textView71);
        editText13 = findViewById(R.id.editText13);
        textView72 = findViewById(R.id.textView72);
        editText14 = findViewById(R.id.editText14);
        textView73 = findViewById(R.id.textView73);
        editText15 = findViewById(R.id.editText15);
        textView74 = findViewById(R.id.textView74);
        editText16 = findViewById(R.id.editText16);

        checkBox5 = findViewById(R.id.checkBox5);
        checkBox6 = findViewById(R.id.checkBox6);
        checkBox7 = findViewById(R.id.checkBox7);
        checkBox8 = findViewById(R.id.checkBox8);
        checkBox10 = findViewById(R.id.checkBox10);

        //навыки мудрости
        textView75 = findViewById(R.id.textView75);
        editText17 = findViewById(R.id.editText17);
        textView76 = findViewById(R.id.textView76);
        editText18 = findViewById(R.id.editText18);
        textView77 = findViewById(R.id.textView77);
        editText19 = findViewById(R.id.editText19);
        textView78 = findViewById(R.id.textView78);
        editText20 = findViewById(R.id.editText20);
        textView79 = findViewById(R.id.textView79);
        editText21 = findViewById(R.id.editText21);

        checkBox11 = findViewById(R.id.checkBox11);
        checkBox12 = findViewById(R.id.checkBox12);
        checkBox13 = findViewById(R.id.checkBox13);
        checkBox14 = findViewById(R.id.checkBox14);
        checkBox15 = findViewById(R.id.checkBox15);

        //навыки харизмы
        textView80 = findViewById(R.id.textView80);
        editText22 = findViewById(R.id.editText22);
        textView81 = findViewById(R.id.textView81);
        editText23 = findViewById(R.id.editText23);
        textView82 = findViewById(R.id.textView82);
        editText24 = findViewById(R.id.editText24);
        textView83 = findViewById(R.id.textView83);
        editText25 = findViewById(R.id.editText25);

        checkBox16 = findViewById(R.id.checkBox16);
        checkBox17 = findViewById(R.id.checkBox17);
        checkBox18 = findViewById(R.id.checkBox18);
        checkBox19 = findViewById(R.id.checkBox19);

        /*Intent intent = getIntent();
        String lvl = intent.getStringExtra("lvl");*/
        checkBonus();

        loadText1();
        loadCheck();

    }

    public void checkBonus(){
        //CharSequence bs = lvl.getText();
        Intent intent = getIntent();
        String lvl = intent.getStringExtra("lvl");

        int bsi= Integer.valueOf(lvl);
        if(bsi >= 1 && bsi <= 4)
            textView12.setText("2");

        if(bsi >= 5 && bsi <= 8)
            textView12.setText("3");

        if(bsi >= 9 && bsi <= 12)
            textView12.setText("4");

        if(bsi >= 13 && bsi <= 16)
            textView12.setText("5");

        if(bsi >= 17 && bsi <= 20)
            textView12.setText("6");
    }

    @SuppressLint("SetTextI18n")
    public void inputmodeStrong(){
        CharSequence num = editText26.getText();
        int pz= Integer.valueOf(num.toString());

        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox.isChecked())
            pz = pz + bsi;

        editText8.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void inputmodelovk(){
        CharSequence num = editText27.getText();
        int pz= Integer.valueOf(num.toString());

        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox2.isChecked()) {
            int pz1 = pz + bsi;
            editText9.setText(Integer.toString(pz1));
        }
        else editText9.setText(Integer.toString(pz));

        if(checkBox3.isChecked()) {
            int pz2 = pz + bsi;
            editText10.setText(Integer.toString(pz2));
        }
        else editText10.setText(Integer.toString(pz));
        if(checkBox4.isChecked()) {
            int pz3 = pz + bsi;
            editText11.setText(Integer.toString(pz3));
        }
        else editText11.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void inputmodeintelekt(){
        CharSequence num = editText29.getText();
        int pz= Integer.valueOf(num.toString());

        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox5.isChecked()) {
            int pz1 = pz + bsi;
            editText12.setText(Integer.toString(pz1));
        }
        else editText12.setText(Integer.toString(pz));

        if(checkBox6.isChecked()) {
            int pz2 = pz + bsi;
            editText13.setText(Integer.toString(pz2));
        }
        else editText13.setText(Integer.toString(pz));

        if(checkBox7.isChecked()) {
            int pz3 = pz + bsi;
            editText14.setText(Integer.toString(pz3));
        }
        else editText14.setText(Integer.toString(pz));

        if(checkBox8.isChecked()) {
            int pz4 = pz + bsi;
            editText15.setText(Integer.toString(pz4));
        }
        else editText15.setText(Integer.toString(pz));

        if(checkBox10.isChecked()) {
            int pz5 = pz + bsi;
            editText16.setText(Integer.toString(pz5));
        }
        else editText16.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void inputmodeMudr(){
        CharSequence num = editText30.getText();
        int pz= Integer.valueOf(num.toString());

        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox11.isChecked()) {
            int pz1 = pz + bsi;
            editText17.setText(Integer.toString(pz1));
        }
        else editText17.setText(num);

        if(checkBox12.isChecked()) {
            int pz2 = pz + bsi;
            editText18.setText(Integer.toString(pz2));
        }
        else editText18.setText(num);

        if(checkBox13.isChecked()) {
            int pz3 = pz + bsi;
            editText19.setText(Integer.toString(pz3));
        }
        else editText19.setText(num);

        if(checkBox14.isChecked()) {
            int pz4 = pz + bsi;
            editText20.setText(Integer.toString(pz4));
        }
        else editText20.setText(num);

        if(checkBox15.isChecked()) {
            int pz5 = pz + bsi;
            editText21.setText(Integer.toString(pz5));
        }
        else editText21.setText(num);
    }
    @SuppressLint("SetTextI18n")
    public void inputmodehar(){
        CharSequence num = editText31.getText();
        int pz= Integer.valueOf(num.toString());

        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox16.isChecked()) {
            int pz1 = pz + bsi;
            editText22.setText(Integer.toString(pz1));
        }
        else editText22.setText(num);

        if(checkBox17.isChecked()) {
            int pz2 = pz + bsi;
            editText23.setText(Integer.toString(pz2));
        }
        else editText23.setText(num);

        if(checkBox18.isChecked()) {
            int pz3 = pz + bsi;
            editText24.setText(Integer.toString(pz3));
        }
        else editText24.setText(num);

        if(checkBox19.isChecked()) {
            int pz4 = pz + bsi;
            editText25.setText(Integer.toString(pz4));
        }
        else editText25.setText(num);

    }

    @SuppressLint("SetTextI18n")
    public void checkStrong(View view) {
        CharSequence num = editText26.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox.isChecked())
            editText8.setText(Integer.toString(pz+bsi));
        else
            editText8.setText(Integer.toString(pz));
    }
    //dexterity
    @SuppressLint("SetTextI18n")
    public void checkLovk1(View view) {
        CharSequence num = editText27.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox2.isChecked())
            editText9.setText(Integer.toString(pz+bsi));
        else
            editText9.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkLovk2(View view) {
        CharSequence num = editText27.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox3.isChecked())
            editText10.setText(Integer.toString(pz+bsi));
        else
            editText10.setText(Integer.toString(pz));
    }

    @SuppressLint("SetTextI18n")
    public void checkLovk3(View view) {
        CharSequence num = editText27.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox4.isChecked())
            editText11.setText(Integer.toString(pz+bsi));
        else
            editText11.setText(Integer.toString(pz));
    }
    //int
    @SuppressLint("SetTextI18n")
    public void checkint1(View view) {
        CharSequence num = editText29.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox5.isChecked())
            editText12.setText(Integer.toString(pz+bsi));
        else
            editText12.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkint2(View view) {
        CharSequence num = editText29.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox6.isChecked())
            editText13.setText(Integer.toString(pz+bsi));
        else
            editText13.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkint3(View view) {
        CharSequence num = editText29.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox7.isChecked())
            editText14.setText(Integer.toString(pz+bsi));
        else
            editText14.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkint4(View view) {
        CharSequence num = editText29.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox8.isChecked())
            editText15.setText(Integer.toString(pz+bsi));
        else
            editText15.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkint5(View view) {
        CharSequence num = editText29.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox10.isChecked())
            editText16.setText(Integer.toString(pz+bsi));
        else
            editText16.setText(Integer.toString(pz));
    }
    //wisdom
    @SuppressLint("SetTextI18n")
    public void checkmudr1(View view) {
        CharSequence num = editText30.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox11.isChecked())
            editText17.setText(Integer.toString(pz+bsi));
        else
            editText17.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkmudr2(View view) {
        CharSequence num = editText30.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox12.isChecked())
            editText18.setText(Integer.toString(pz+bsi));
        else
            editText18.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkmudr3(View view) {
        CharSequence num = editText30.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox13.isChecked())
            editText19.setText(Integer.toString(pz+bsi));
        else
            editText19.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkmudr4(View view) {
        CharSequence num = editText30.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox14.isChecked())
            editText20.setText(Integer.toString(pz+bsi));
        else
            editText20.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkmudr5(View view) {
        CharSequence num = editText30.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox15.isChecked())
            editText21.setText(Integer.toString(pz+bsi));
        else
            editText21.setText(Integer.toString(pz));
    }
    //charisma
    @SuppressLint("SetTextI18n")
    public void checkchar1(View view) {
        CharSequence num = editText31.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox16.isChecked())
            editText22.setText(Integer.toString(pz+bsi));
        else
            editText22.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkchar2(View view) {
        CharSequence num = editText31.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox17.isChecked())
            editText23.setText(Integer.toString(pz+bsi));
        else
            editText23.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkchar3(View view) {
        CharSequence num = editText31.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox18.isChecked())
            editText24.setText(Integer.toString(pz+bsi));
        else
            editText24.setText(Integer.toString(pz));
    }
    @SuppressLint("SetTextI18n")
    public void checkchar4(View view) {
        CharSequence num = editText31.getText();
        int pz= Integer.valueOf(num.toString());
        CharSequence bs = textView12.getText();
        int bsi= Integer.valueOf(bs.toString());
        if(checkBox19.isChecked())
            editText25.setText(Integer.toString(pz+bsi));
        else
            editText25.setText(Integer.toString(pz));
    }

    public void saveCheck(){
        //strong
        if(checkBox.isChecked()){
            bunoCheck = getSharedPreferences("bunCheck", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck = getSharedPreferences("bunCheck", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        //dexterity
        if(checkBox2.isChecked()){
            bunoCheck2 = getSharedPreferences("bunCheck2", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck2.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck2 = getSharedPreferences("bunCheck2", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck2.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox3.isChecked()){
            bunoCheck3 = getSharedPreferences("bunCheck3", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck3.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck3 = getSharedPreferences("bunCheck3", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck3.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox4.isChecked()){
            bunoCheck4 = getSharedPreferences("bunCheck4", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck4.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck4 = getSharedPreferences("bunCheck4", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck4.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        //intelligence
        if(checkBox5.isChecked()){
            bunoCheck5 = getSharedPreferences("bunCheck5", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck5.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck5 = getSharedPreferences("bunCheck5", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck5.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox6.isChecked()){
            bunoCheck6 = getSharedPreferences("bunCheck6", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck6.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck6 = getSharedPreferences("bunCheck6", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck6.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox7.isChecked()){
            bunoCheck7 = getSharedPreferences("bunCheck7", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck7.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck7 = getSharedPreferences("bunCheck7", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck7.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox8.isChecked()){
            bunoCheck8 = getSharedPreferences("bunCheck8", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck8.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck8 = getSharedPreferences("bunCheck8", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck8.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox10.isChecked()){
            bunoCheck10 = getSharedPreferences("bunCheck10", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck10.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck10 = getSharedPreferences("bunCheck10", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck10.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        //wisdom
        if(checkBox11.isChecked()){
            bunoCheck11 = getSharedPreferences("bunCheck11", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck11.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck11 = getSharedPreferences("bunCheck11", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck11.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox12.isChecked()){
            bunoCheck12 = getSharedPreferences("bunCheck12", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck12.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck12 = getSharedPreferences("bunCheck12", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck12.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox13.isChecked()){
            bunoCheck13 = getSharedPreferences("bunCheck13", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck13.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck13 = getSharedPreferences("bunCheck13", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck13.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox14.isChecked()){
            bunoCheck14 = getSharedPreferences("bunCheck14", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck14.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck14 = getSharedPreferences("bunCheck14", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck14.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox15.isChecked()){
            bunoCheck15 = getSharedPreferences("bunCheck15", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck15.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck15 = getSharedPreferences("bunCheck15", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck15.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        //charisma
        if(checkBox16.isChecked()){
            bunoCheck16 = getSharedPreferences("bunCheck16", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck16.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck16 = getSharedPreferences("bunCheck16", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck16.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox17.isChecked()){
            bunoCheck17 = getSharedPreferences("bunCheck17", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck17.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck17 = getSharedPreferences("bunCheck17", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck17.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox18.isChecked()){
            bunoCheck18 = getSharedPreferences("bunCheck18", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck18.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck18 = getSharedPreferences("bunCheck18", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck18.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }
        if(checkBox19.isChecked()){
            bunoCheck19 = getSharedPreferences("bunCheck19", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck19.edit();
            ed.putString(SAVED_TEXT1, "true");
            ed.apply();
        }
        else{
            bunoCheck19 = getSharedPreferences("bunCheck19", MODE_PRIVATE);
            SharedPreferences.Editor ed = bunoCheck19.edit();
            ed.putString(SAVED_TEXT1, "false");
            ed.apply();
        }


    }   //checkBox2.setChecked(true);
    public void loadCheck(){
        bunoCheck = getSharedPreferences("bunCheck", MODE_PRIVATE);
        String savedCheck1 = bunoCheck.getString(SAVED_TEXT1, "false");
        checkBox.setChecked(Boolean.valueOf(savedCheck1));

        bunoCheck2 = getSharedPreferences("bunCheck2", MODE_PRIVATE);
        String savedCheck2 = bunoCheck2.getString(SAVED_TEXT1, "false");
        checkBox2.setChecked(Boolean.valueOf(savedCheck2));

        bunoCheck3 = getSharedPreferences("bunCheck3", MODE_PRIVATE);
        String savedCheck3 = bunoCheck3.getString(SAVED_TEXT1, "false");
        checkBox3.setChecked(Boolean.valueOf(savedCheck3));

        bunoCheck4 = getSharedPreferences("bunCheck4", MODE_PRIVATE);
        String savedCheck4 = bunoCheck4.getString(SAVED_TEXT1, "false");
        checkBox4.setChecked(Boolean.valueOf(savedCheck4));

        bunoCheck5 = getSharedPreferences("bunCheck5", MODE_PRIVATE);
        String savedCheck5 = bunoCheck5.getString(SAVED_TEXT1, "false");
        checkBox5.setChecked(Boolean.valueOf(savedCheck5));

        bunoCheck6 = getSharedPreferences("bunCheck6", MODE_PRIVATE);
        String savedCheck6 = bunoCheck6.getString(SAVED_TEXT1, "false");
        checkBox6.setChecked(Boolean.valueOf(savedCheck6));

        bunoCheck7 = getSharedPreferences("bunCheck7", MODE_PRIVATE);
        String savedCheck7 = bunoCheck7.getString(SAVED_TEXT1, "false");
        checkBox7.setChecked(Boolean.valueOf(savedCheck7));

        bunoCheck8 = getSharedPreferences("bunCheck8", MODE_PRIVATE);
        String savedCheck8 = bunoCheck8.getString(SAVED_TEXT1, "false");
        checkBox8.setChecked(Boolean.valueOf(savedCheck8));

        bunoCheck10 = getSharedPreferences("bunCheck10", MODE_PRIVATE);
        String savedCheck10 = bunoCheck10.getString(SAVED_TEXT1, "false");
        checkBox10.setChecked(Boolean.valueOf(savedCheck10));

        bunoCheck11 = getSharedPreferences("bunCheck11", MODE_PRIVATE);
        String savedCheck11 = bunoCheck11.getString(SAVED_TEXT1, "false");
        checkBox11.setChecked(Boolean.valueOf(savedCheck11));

        bunoCheck12 = getSharedPreferences("bunCheck12", MODE_PRIVATE);
        String savedCheck12 = bunoCheck12.getString(SAVED_TEXT1, "false");
        checkBox12.setChecked(Boolean.valueOf(savedCheck12));

        bunoCheck13 = getSharedPreferences("bunCheck13", MODE_PRIVATE);
        String savedCheck13 = bunoCheck13.getString(SAVED_TEXT1, "false");
        checkBox13.setChecked(Boolean.valueOf(savedCheck13));

        bunoCheck14 = getSharedPreferences("bunCheck14", MODE_PRIVATE);
        String savedCheck14 = bunoCheck14.getString(SAVED_TEXT1, "false");
        checkBox14.setChecked(Boolean.valueOf(savedCheck14));

        bunoCheck15 = getSharedPreferences("bunCheck15", MODE_PRIVATE);
        String savedCheck15 = bunoCheck10.getString(SAVED_TEXT1, "false");
        checkBox15.setChecked(Boolean.valueOf(savedCheck15));

        bunoCheck16 = getSharedPreferences("bunCheck16", MODE_PRIVATE);
        String savedCheck16 = bunoCheck16.getString(SAVED_TEXT1, "false");
        checkBox16.setChecked(Boolean.valueOf(savedCheck16));

        bunoCheck17 = getSharedPreferences("bunCheck17", MODE_PRIVATE);
        String savedCheck17 = bunoCheck17.getString(SAVED_TEXT1, "false");
        checkBox17.setChecked(Boolean.valueOf(savedCheck17));

        bunoCheck18 = getSharedPreferences("bunCheck18", MODE_PRIVATE);
        String savedCheck18 = bunoCheck18.getString(SAVED_TEXT1, "false");
        checkBox18.setChecked(Boolean.valueOf(savedCheck18));

        bunoCheck19 = getSharedPreferences("bunCheck19", MODE_PRIVATE);
        String savedCheck19 = bunoCheck19.getString(SAVED_TEXT1, "false");
        checkBox19.setChecked(Boolean.valueOf(savedCheck19));
    }

    @SuppressLint("SetTextI18n")
    public void bun(View view) {
        CharSequence zz = button16.getText(); // получем содержимое обьекта
        int pz= Integer.valueOf(zz.toString()); // преобразовываем в число
        pz=pz-1;
        int mod = (pz-10)/2;
        button16.setText(Integer.toString(pz));// преобразовываем в строку и возвращаем в обьект "надпись"
        editText26.setText(Integer.toString(mod));
        inputmodeStrong();
    }
    @SuppressLint("SetTextI18n")
    public void bun2(View view) {
        CharSequence zz = button16.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz+1;
        int mod = (pz-10)/2;
        button16.setText(Integer.toString(pz));
        editText26.setText(Integer.toString(mod));
        inputmodeStrong();
    }

    @SuppressLint("SetTextI18n")
    public void bun3(View view) {
        CharSequence zz = button17.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz-1;
        int mod = (pz-10)/2;
        editText27.setText(Integer.toString(mod));
        button17.setText(Integer.toString(pz));
        inputmodelovk();
    }
    @SuppressLint("SetTextI18n")
    public void bun4(View view) {
        CharSequence zz = button17.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz+1;
        int mod = (pz-10)/2;
        editText27.setText(Integer.toString(mod));
        button17.setText(Integer.toString(pz));
        inputmodelovk();
    }

    @SuppressLint("SetTextI18n")
    public void bun5(View view) {
        CharSequence zz = button18.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz-1;
        int mod = (pz-10)/2;
        editText28.setText(Integer.toString(mod));
        button18.setText(Integer.toString(pz));

    }
    @SuppressLint("SetTextI18n")
    public void bun6(View view) {
        CharSequence zz = button18.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz+1;
        int mod = (pz-10)/2;
        editText28.setText(Integer.toString(mod));
        button18.setText(Integer.toString(pz));

    }

    @SuppressLint("SetTextI18n")
    public void bun7(View view) {
        CharSequence zz = button19.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz-1;
        int mod = (pz-10)/2;
        editText29.setText(Integer.toString(mod));
        button19.setText(Integer.toString(pz));
        inputmodeintelekt();
    }
    @SuppressLint("SetTextI18n")
    public void bun8(View view) {
        CharSequence zz = button19.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz+1;
        int mod = (pz-10)/2;
        editText29.setText(Integer.toString(mod));
        button19.setText(Integer.toString(pz));
        inputmodeintelekt();
    }

    @SuppressLint("SetTextI18n")
    public void bun9(View view) {
        CharSequence zz = button20.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz-1;
        int mod = (pz-10)/2;
        editText30.setText(Integer.toString(mod));
        button20.setText(Integer.toString(pz));
        inputmodeMudr();
    }
    @SuppressLint("SetTextI18n")
    public void bun10(View view) {
        CharSequence zz = button20.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz+1;
        int mod = (pz-10)/2;
        editText30.setText(Integer.toString(mod));
        button20.setText(Integer.toString(pz));
        inputmodeMudr();
    }

    @SuppressLint("SetTextI18n")
    public void bun11(View view) {
        CharSequence zz = button21.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz-1;
        int mod = (pz-10)/2;
        editText31.setText(Integer.toString(mod));
        button21.setText(Integer.toString(pz));
        inputmodehar();
    }
    @SuppressLint("SetTextI18n")
    public void bun12(View view) {
        CharSequence zz = button21.getText();
        int pz= Integer.valueOf(zz.toString());
        pz=pz+1;
        int mod = (pz-10)/2;
        editText31.setText(Integer.toString(mod));
        button21.setText(Integer.toString(pz));
        inputmodehar();
    }

    private void saveText1() {
        buno = getSharedPreferences("bun1", MODE_PRIVATE);
        SharedPreferences.Editor ed = buno.edit();
        ed.putString(SAVED_TEXT1, button16.getText().toString());
        ed.apply();

        buno2 = getSharedPreferences("bun2", MODE_PRIVATE);
        SharedPreferences.Editor ed1 = buno2.edit();
        ed1.putString(SAVED_TEXT1, button17.getText().toString());
        ed1.apply();

        buno3 = getSharedPreferences("bun3", MODE_PRIVATE);
        SharedPreferences.Editor ed2 = buno3.edit();
        ed2.putString(SAVED_TEXT1, button18.getText().toString());
        ed2.apply();

        buno4 = getSharedPreferences("bun4", MODE_PRIVATE);
        SharedPreferences.Editor ed3 = buno4.edit();
        ed3.putString(SAVED_TEXT1, button19.getText().toString());
        ed3.apply();

        buno5 = getSharedPreferences("bun5", MODE_PRIVATE);
        SharedPreferences.Editor ed4 = buno5.edit();
        ed4.putString(SAVED_TEXT1, button20.getText().toString());
        ed4.apply();

        buno6 = getSharedPreferences("bun6", MODE_PRIVATE);
        SharedPreferences.Editor ed5 = buno6.edit();
        ed5.putString(SAVED_TEXT1, button21.getText().toString());
        ed5.apply();
        //навыки

        buno8 = getSharedPreferences("bun8", MODE_PRIVATE);
        SharedPreferences.Editor ed8 = buno8.edit();
        ed8.putString(SAVED_TEXT1, editText8.getText().toString());
        ed8.apply();

        buno9 = getSharedPreferences("bun9", MODE_PRIVATE);
        SharedPreferences.Editor ed9 = buno9.edit();
        ed9.putString(SAVED_TEXT1, editText9.getText().toString());
        ed9.apply();

        buno10 = getSharedPreferences("bun10", MODE_PRIVATE);
        SharedPreferences.Editor ed10 = buno10.edit();
        ed10.putString(SAVED_TEXT1, editText10.getText().toString());
        ed10.apply();

        buno11 = getSharedPreferences("bun11", MODE_PRIVATE);
        SharedPreferences.Editor ed11 = buno11.edit();
        ed11.putString(SAVED_TEXT1, editText11.getText().toString());
        ed11.apply();

        buno12 = getSharedPreferences("bun12", MODE_PRIVATE);
        SharedPreferences.Editor ed12 = buno12.edit();
        ed12.putString(SAVED_TEXT1, editText12.getText().toString());
        ed12.apply();

        buno13 = getSharedPreferences("bun13", MODE_PRIVATE);
        SharedPreferences.Editor ed13 = buno13.edit();
        ed13.putString(SAVED_TEXT1, editText13.getText().toString());
        ed13.apply();

        buno14 = getSharedPreferences("bun14", MODE_PRIVATE);
        SharedPreferences.Editor ed14 = buno14.edit();
        ed14.putString(SAVED_TEXT1, editText14.getText().toString());
        ed14.apply();

        buno15 = getSharedPreferences("bun15", MODE_PRIVATE);
        SharedPreferences.Editor ed15 = buno15.edit();
        ed15.putString(SAVED_TEXT1, editText15.getText().toString());
        ed15.apply();

        buno16 = getSharedPreferences("bun16", MODE_PRIVATE);
        SharedPreferences.Editor ed16 = buno16.edit();
        ed16.putString(SAVED_TEXT1, editText16.getText().toString());
        ed16.apply();

        buno17 = getSharedPreferences("bun17", MODE_PRIVATE);
        SharedPreferences.Editor ed17 = buno17.edit();
        ed17.putString(SAVED_TEXT1, editText17.getText().toString());
        ed17.apply();

        buno18 = getSharedPreferences("bun18", MODE_PRIVATE);
        SharedPreferences.Editor ed18 = buno18.edit();
        ed18.putString(SAVED_TEXT1, editText18.getText().toString());
        ed18.apply();

        buno19 = getSharedPreferences("bun19", MODE_PRIVATE);
        SharedPreferences.Editor ed19 = buno19.edit();
        ed19.putString(SAVED_TEXT1, editText19.getText().toString());
        ed19.apply();

        buno20 = getSharedPreferences("bun20", MODE_PRIVATE);
        SharedPreferences.Editor ed20 = buno20.edit();
        ed20.putString(SAVED_TEXT1, editText20.getText().toString());
        ed20.apply();

        buno21 = getSharedPreferences("bun21", MODE_PRIVATE);
        SharedPreferences.Editor ed21 = buno21.edit();
        ed21.putString(SAVED_TEXT1, editText21.getText().toString());
        ed21.apply();

        buno22 = getSharedPreferences("bun22", MODE_PRIVATE);
        SharedPreferences.Editor ed22 = buno22.edit();
        ed22.putString(SAVED_TEXT1, editText22.getText().toString());
        ed22.apply();

        buno23 = getSharedPreferences("bun23", MODE_PRIVATE);
        SharedPreferences.Editor ed23 = buno23.edit();
        ed23.putString(SAVED_TEXT1, editText23.getText().toString());
        ed23.apply();

        buno24 = getSharedPreferences("bun24", MODE_PRIVATE);
        SharedPreferences.Editor ed24 = buno24.edit();
        ed24.putString(SAVED_TEXT1, editText24.getText().toString());
        ed24.apply();

        buno25 = getSharedPreferences("bun25", MODE_PRIVATE);
        SharedPreferences.Editor ed25 = buno25.edit();
        ed25.putString(SAVED_TEXT1, editText25.getText().toString());
        ed25.apply();

        buno26 = getSharedPreferences("bun26", MODE_PRIVATE);
        SharedPreferences.Editor ed26 = buno26.edit();
        ed26.putString(SAVED_TEXT1, editText26.getText().toString());
        ed26.apply();

        buno27 = getSharedPreferences("bun27", MODE_PRIVATE);
        SharedPreferences.Editor ed27 = buno27.edit();
        ed27.putString(SAVED_TEXT1, editText27.getText().toString());
        ed27.apply();

        buno28 = getSharedPreferences("bun28", MODE_PRIVATE);
        SharedPreferences.Editor ed28 = buno28.edit();
        ed28.putString(SAVED_TEXT1, editText28.getText().toString());
        ed28.apply();

        buno29 = getSharedPreferences("bun29", MODE_PRIVATE);
        SharedPreferences.Editor ed29 = buno29.edit();
        ed29.putString(SAVED_TEXT1, editText29.getText().toString());
        ed29.apply();

        buno30 = getSharedPreferences("bun30", MODE_PRIVATE);
        SharedPreferences.Editor ed30 = buno30.edit();
        ed30.putString(SAVED_TEXT1, editText30.getText().toString());
        ed30.apply();

        buno31 = getSharedPreferences("bun31", MODE_PRIVATE);
        SharedPreferences.Editor ed31 = buno31.edit();
        ed31.putString(SAVED_TEXT1, editText31.getText().toString());
        ed31.apply();

        /*buno32 = getSharedPreferences("bun32", MODE_PRIVATE);
        SharedPreferences.Editor ed32 = buno32.edit();
        ed32.putString(SAVED_TEXT1, lvl.getText().toString());
        ed32.apply();*/
    }
    private void loadText1() {
        buno = getSharedPreferences("bun1", MODE_PRIVATE);
        String savedText1 = buno.getString(SAVED_TEXT1, "0");
        button16.setText(savedText1);
        buno2 = getSharedPreferences("bun2", MODE_PRIVATE);
        String savedText2 = buno2.getString(SAVED_TEXT1, "0");
        button17.setText(savedText2);
        buno3 = getSharedPreferences("bun3", MODE_PRIVATE);
        String savedText3 = buno3.getString(SAVED_TEXT1, "0");
        button18.setText(savedText3);
        buno4 = getSharedPreferences("bun4", MODE_PRIVATE);
        String savedText4 = buno4.getString(SAVED_TEXT1, "0");
        button19.setText(savedText4);
        buno5 = getSharedPreferences("bun5", MODE_PRIVATE);
        String savedText5 = buno5.getString(SAVED_TEXT1, "0");
        button20.setText(savedText5);
        buno6 = getSharedPreferences("bun6", MODE_PRIVATE);
        String savedText6 = buno6.getString(SAVED_TEXT1, "0");
        button21.setText(savedText6);
        //навыки

        buno8 = getSharedPreferences("bun8", MODE_PRIVATE);
        String savedText8 = buno8.getString(SAVED_TEXT1, "0");
        editText8.setText(savedText8);
        buno9 = getSharedPreferences("bun9", MODE_PRIVATE);
        String savedText9 = buno9.getString(SAVED_TEXT1, "0");
        editText9.setText(savedText9);
        buno10 = getSharedPreferences("bun10", MODE_PRIVATE);
        String savedText10 = buno10.getString(SAVED_TEXT1, "0");
        editText10.setText(savedText10);
        buno11 = getSharedPreferences("bun11", MODE_PRIVATE);
        String savedText11 = buno11.getString(SAVED_TEXT1, "0");
        editText11.setText(savedText11);
        buno12 = getSharedPreferences("bun12", MODE_PRIVATE);
        String savedText12 = buno12.getString(SAVED_TEXT1, "0");
        editText12.setText(savedText12);
        buno13 = getSharedPreferences("bun13", MODE_PRIVATE);
        String savedText13 = buno13.getString(SAVED_TEXT1, "0");
        editText13.setText(savedText13);
        buno14 = getSharedPreferences("bun14", MODE_PRIVATE);
        String savedText14 = buno14.getString(SAVED_TEXT1, "0");
        editText14.setText(savedText14);
        buno15 = getSharedPreferences("bun15", MODE_PRIVATE);
        String savedText15 = buno15.getString(SAVED_TEXT1, "0");
        editText15.setText(savedText15);
        buno16 = getSharedPreferences("bun16", MODE_PRIVATE);
        String savedText16 = buno16.getString(SAVED_TEXT1, "0");
        editText16.setText(savedText16);
        buno17 = getSharedPreferences("bun17", MODE_PRIVATE);
        String savedText17 = buno17.getString(SAVED_TEXT1, "0");
        editText17.setText(savedText17);
        buno18 = getSharedPreferences("bun18", MODE_PRIVATE);
        String savedText18 = buno18.getString(SAVED_TEXT1, "0");
        editText18.setText(savedText18);
        buno19 = getSharedPreferences("bun19", MODE_PRIVATE);
        String savedText19 = buno19.getString(SAVED_TEXT1, "0");
        editText19.setText(savedText19);
        buno20 = getSharedPreferences("bun20", MODE_PRIVATE);
        String savedText20 = buno20.getString(SAVED_TEXT1, "0");
        editText20.setText(savedText20);
        buno21 = getSharedPreferences("bun21", MODE_PRIVATE);
        String savedText21 = buno21.getString(SAVED_TEXT1, "0");
        editText21.setText(savedText21);
        buno22 = getSharedPreferences("bun22", MODE_PRIVATE);
        String savedText22 = buno22.getString(SAVED_TEXT1, "0");
        editText22.setText(savedText22);
        buno23 = getSharedPreferences("bun23", MODE_PRIVATE);
        String savedText23 = buno23.getString(SAVED_TEXT1, "0");
        editText23.setText(savedText23);
        buno24 = getSharedPreferences("bun24", MODE_PRIVATE);
        String savedText24 = buno24.getString(SAVED_TEXT1, "0");
        editText24.setText(savedText24);
        buno25 = getSharedPreferences("bun25", MODE_PRIVATE);
        String savedText25 = buno25.getString(SAVED_TEXT1, "0");
        editText25.setText(savedText25);
        buno26 = getSharedPreferences("bun26", MODE_PRIVATE);
        String savedText26 = buno26.getString(SAVED_TEXT1, "0");
        editText26.setText(savedText26);
        buno27 = getSharedPreferences("bun27", MODE_PRIVATE);
        String savedText27 = buno27.getString(SAVED_TEXT1, "0");
        editText27.setText(savedText27);
        buno28 = getSharedPreferences("bun28", MODE_PRIVATE);
        String savedText28 = buno28.getString(SAVED_TEXT1, "0");
        editText28.setText(savedText28);
        buno29 = getSharedPreferences("bun29", MODE_PRIVATE);
        String savedText29 = buno29.getString(SAVED_TEXT1, "0");
        editText29.setText(savedText29);
        buno30 = getSharedPreferences("bun30", MODE_PRIVATE);
        String savedText30 = buno30.getString(SAVED_TEXT1, "0");
        editText30.setText(savedText30);
        buno31 = getSharedPreferences("bun31", MODE_PRIVATE);
        String savedText31 = buno31.getString(SAVED_TEXT1, "0");
        editText31.setText(savedText31);

        /*buno32 = getSharedPreferences("bun32", MODE_PRIVATE);
        String savedText32 = buno32.getString(SAVED_TEXT1, "0");
        lvl.setText(savedText32);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveText1();
        saveCheck();
    }

    @Override
    public void onPause() {
        super.onPause();
        saveText1();
        saveCheck();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // strong editText26
        //lovk editText27
        //intelect editText29
        //mudr editText30
        //chrarisma editText31
        //telosloz editText28
        Intent intent = new Intent(activity_main_game_stats.this, activity_main_game.class);
        intent.putExtra("stat_strong", editText26.getText().toString());
        intent.putExtra("stat_lovk", editText27.getText().toString());
        intent.putExtra("stat_intelect", editText29.getText().toString());
        intent.putExtra("stat_mudr", editText30.getText().toString());
        intent.putExtra("stat_charisma", editText31.getText().toString());
        intent.putExtra("stat_telosloz", editText28.getText().toString());
        startActivity(intent);
    }
}
