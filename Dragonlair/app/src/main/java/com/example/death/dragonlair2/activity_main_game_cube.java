package com.example.death.dragonlair2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class activity_main_game_cube extends AppCompatActivity {

    ImageView imageView, imageView5, imageView7, imageView14, imageView15, imageView18;

    Animation mFadeInAnimation, mFadeOutAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game_cube);

        imageView = findViewById(R.id.imageView);
        imageView5 = findViewById(R.id.imageView5);
        imageView7 = findViewById(R.id.imageView7);
        imageView14 = findViewById(R.id.imageView14);
        imageView15 = findViewById(R.id.imageView15);
        imageView18 = findViewById(R.id.imageView18);

        mFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.animin);
        mFadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.animout);

    }

    //anim d12
    public void animcube(View view){
        final Animation animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.rotate_center);
        imageView.startAnimation(animationRotateCenter);
        animationRotateCenter.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                TextView randD12 = findViewById(R.id.randD12);
                randD12.startAnimation(mFadeOutAnimation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Random rand3 = new Random();
                int number3 = rand3.nextInt(12)+1;
                TextView randD12 = findViewById(R.id.randD12);
                String string3 = String.valueOf(number3);
                randD12.setText(string3);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
    //anim d6
    public void animcube1(View view){
        final Animation animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.rotate_center);
        imageView5.startAnimation(animationRotateCenter);
        animationRotateCenter.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                TextView randD6 = findViewById(R.id.randD6);
                randD6.startAnimation(mFadeOutAnimation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Random rand2 = new Random();
                int number2 = rand2.nextInt(6)+1;
                TextView randD6 = findViewById(R.id.randD6);
                String string2 = String.valueOf(number2);
                randD6.setText(string2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
    //anim d20 вместо d4
    public void animcube2(View view){
        final Animation animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.rotate_center);
        imageView7.startAnimation(animationRotateCenter);
        animationRotateCenter.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                TextView randD4 = findViewById(R.id.randD4);
                randD4.startAnimation(mFadeOutAnimation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Random rand = new Random();
                int number = rand.nextInt(20)+1;
                TextView randD4 = findViewById(R.id.randD4);
                String string = String.valueOf(number);
                randD4.setText(string);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    //anim d10
    public void animcube3(View view){
        final Animation animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.rotate_center);
        imageView14.startAnimation(animationRotateCenter);
        animationRotateCenter.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                TextView textView21 = findViewById(R.id.textView21);
                textView21.startAnimation(mFadeOutAnimation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Random rand = new Random();
                int number = rand.nextInt(10)+1;
                TextView textView21 = findViewById(R.id.textView21);
                String string = String.valueOf(number);
                textView21.setText(string);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    //anim d8
    public void animcube4(View view){
        final Animation animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.rotate_center);
        imageView15.startAnimation(animationRotateCenter);
        animationRotateCenter.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                TextView textView59 = findViewById(R.id.textView59);
                textView59.startAnimation(mFadeOutAnimation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Random rand = new Random();
                int number = rand.nextInt(8)+1;
                TextView textView59 = findViewById(R.id.textView59);
                String string = String.valueOf(number);
                textView59.setText(string);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    //anim d4
    public void animcube5(View view){
        final Animation animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.rotate_center);
        imageView18.startAnimation(animationRotateCenter);
        animationRotateCenter.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                TextView textView60 = findViewById(R.id.textView60);
                textView60.startAnimation(mFadeOutAnimation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Random rand = new Random();
                int number = rand.nextInt(4)+1;
                TextView textView60 = findViewById(R.id.textView60);
                String string = String.valueOf(number);
                textView60.setText(string);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(activity_main_game_cube.this, activity_main_game.class);
        startActivity(intent);
    }
}
