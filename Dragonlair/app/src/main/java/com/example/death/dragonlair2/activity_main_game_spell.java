package com.example.death.dragonlair2;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class activity_main_game_spell extends AppCompatActivity {

    private ListView list_spell;
    private DatabaseHelper databaseHelper;
    private List<Friend> friendList;

    final Context context = this;

    SharedPreferences sPref1_temp_dat;
    final String SAVED_TEXT1 = "saved_text";
    final String temp_dat = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game_spell);

        list_spell = findViewById(R.id.list_spell);

        databaseHelper = new DatabaseHelper(this);
        friendList = new ArrayList<>();

        check_data();

        SharedPreferences temp_database = this.getSharedPreferences("MyPrefA_temp_dat", Context.MODE_PRIVATE);
        temp_database.edit().clear().apply();

        reloadingDatabase();
    }

    @SuppressLint("SetTextI18n")
    public void reloadingDatabase() {
        friendList = databaseHelper.getAllFriends();
        ListViewAdapter adapter = new ListViewAdapter(this, R.layout.list_item, friendList, databaseHelper);
        list_spell.setAdapter(adapter);               //ЧЁ ТА ТУТ ПОХОДУ
    }

    //adding data
    public void show_alert(View view) {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert);
        Button dialogButton = dialog.findViewById(R.id.but_ok_al);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                EditText editnamespell = dialog.findViewById(R.id.editnamespell);
                EditText editdescriptionspell = dialog.findViewById(R.id.editdescriptionspell);

                String name_spell = editnamespell.getText().toString();
                String description_spell = editdescriptionspell.getText().toString();
                if(!name_spell.isEmpty()){

                    //Adding in database spell
                    Friend friend = new Friend(name_spell, description_spell);
                    databaseHelper.addNewFriend(friend);

                    reloadingDatabase(); //reload the db to view

                    Toast.makeText(getApplicationContext(),name_spell,Toast.LENGTH_SHORT).show();
                }

            }
        });
        dialog.show();
        Button but_cancel_al = dialog.findViewById(R.id.but_cancel_al);
        but_cancel_al.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),"Close..!!",Toast.LENGTH_SHORT).show();
            }
        });


    }

    //check data
    public void check_data(){
        sPref1_temp_dat = getSharedPreferences("MyPrefA_temp_dat", MODE_PRIVATE);
        String saved_datas = sPref1_temp_dat.getString(SAVED_TEXT1, "0");

        if(temp_dat.equals(saved_datas)){
            databaseHelper.deleteFriend_all();
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(activity_main_game_spell.this, activity_main_game.class);
        startActivity(intent);
    }
}
