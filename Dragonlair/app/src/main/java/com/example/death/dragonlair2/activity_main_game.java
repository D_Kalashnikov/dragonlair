package com.example.death.dragonlair2;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;

public class activity_main_game extends TabActivity {

    //Button but_menu, but_menu_arrow;
    TextView txtInitiative, txtEditInitiative, txtArmClass, txtEditArmClass, txtLvl, txtEditLvl,
            txtRun1, txtEditRun, txtEditName, txtMelee, txtRange, txtSpell,
            txtEditMelee, txtEditRange, txtEditSpell,
            txtGold, txtHealth, txtArmor, txtDamage,
            txtEditWisdom, txtEditCharisma, txtEditMind, txtEditAgility, txtEditPhysique, txtEditForce;
    EditText editAboutBlock, textView20, textView16, textView18, textView23;
    RelativeLayout hidetab;
    ScrollView scrolview1;

    private static int IMG_RESULT = 1;
    CircleImageView imageAvatar;
    String ImageDecode;
    final Context context = this;

    SharedPreferences buno26, buno27, buno28, buno29, buno30, buno31,
    sPref1_name, sPref5_healt, sPref6_run, sPref5_healt_change,
    sPref_lvl,  sPref_melee, sPref_range,  sPref_spell,
    sPref_gold, sPref_armor, sPref_damage,
    SP_avatar, sPref_note;


    final String SAVED_TEXT1 = "saved_text";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game);

        imageAvatar = findViewById(R.id.imageAvatar);

        txtInitiative = findViewById(R.id.txtInitiative);
        txtArmClass = findViewById(R.id.txtArmClass);
        txtEditArmClass = findViewById(R.id.txtEditArmClass);

        txtLvl = findViewById(R.id.txtLvl);
        txtEditLvl = findViewById(R.id.txtEditLvl);

        txtRun1 = findViewById(R.id.txtRun1);
        txtEditRun = findViewById(R.id.txtEditRun);

        txtEditName = findViewById(R.id.txtEditName);

        txtMelee = findViewById(R.id.txtMelee);
        txtRange = findViewById(R.id.txtRange);
        txtSpell = findViewById(R.id.txtSpell);

        txtEditRange = findViewById(R.id.txtEditRange);
        txtEditSpell = findViewById(R.id.txtEditSpell);

        editAboutBlock = findViewById(R.id.editAboutBlock);

        scrolview1 = findViewById(R.id.scrolview1);

        txtEditWisdom = findViewById(R.id.txtEditWisdom);
        txtEditCharisma = findViewById(R.id.txtEditCharisma);
        txtEditMind = findViewById(R.id.txtEditMind);
        txtEditAgility = findViewById(R.id.txtEditAgility);
        txtEditPhysique = findViewById(R.id.txtEditPhysique);
        txtEditForce = findViewById(R.id.txtEditForce);

        textView16 = findViewById(R.id.textView16);
        textView18 = findViewById(R.id.textView18);
        textView20 = findViewById(R.id.textView20);
        textView23 = findViewById(R.id.textView23);

        txtEditMelee = (EditText)findViewById(R.id.txtEditMelee);
        txtEditRange = (EditText)findViewById(R.id.txtEditRange);
        txtEditSpell = (EditText)findViewById(R.id.txtEditSpell);

        Intent intent = getIntent();
        String stat_strong = intent.getStringExtra("stat_strong");
        String stat_lovk = intent.getStringExtra("stat_lovk");
        String stat_intelect = intent.getStringExtra("stat_intelect");
        String stat_mudr = intent.getStringExtra("stat_mudr");
        String stat_charisma = intent.getStringExtra("stat_charisma");
        String stat_telosloz = intent.getStringExtra("stat_telosloz");

        String new_name = intent.getStringExtra("new_name");
        String new_healt = intent.getStringExtra("new_healt");
        String new_run = intent.getStringExtra("new_run");

        txtEditWisdom.setText(stat_mudr);
        txtEditCharisma.setText(stat_charisma);
        txtEditMind.setText(stat_intelect);
        txtEditAgility.setText(stat_lovk);
        txtEditPhysique.setText(stat_telosloz);
        txtEditForce.setText(stat_strong);

        buno26 = getSharedPreferences("bun26", MODE_PRIVATE);
        String savedText26 = buno26.getString(SAVED_TEXT1, "0");
        txtEditForce.setText(savedText26);
        buno27 = getSharedPreferences("bun27", MODE_PRIVATE);
        String savedText27 = buno27.getString(SAVED_TEXT1, "0");
        txtEditAgility.setText(savedText27);
        buno28 = getSharedPreferences("bun28", MODE_PRIVATE);
        String savedText28 = buno28.getString(SAVED_TEXT1, "0");
        txtEditPhysique.setText(savedText28);
        buno29 = getSharedPreferences("bun29", MODE_PRIVATE);
        String savedText29 = buno29.getString(SAVED_TEXT1, "0");
        txtEditMind.setText(savedText29);
        buno30 = getSharedPreferences("bun30", MODE_PRIVATE);
        String savedText30 = buno30.getString(SAVED_TEXT1, "0");
        txtEditWisdom.setText(savedText30);
        buno31 = getSharedPreferences("bun31", MODE_PRIVATE);
        String savedText31 = buno31.getString(SAVED_TEXT1, "0");
        txtEditCharisma.setText(savedText31);

        txtEditName.setText(new_name);
        textView16.setText(new_healt);
        txtEditRun.setText(new_run);

        load_lvl();
        load_attack();
        load_save_stat();
        load_HGAD();
        load_variable_stats();

        load_avatar();

        textView20.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
               String ifarm = textView20.getText().toString();
               if(!ifarm.equals("")){ load_variable_stats(); }
            }
        });

        final FrameLayout tabcontent = findViewById(android.R.id.tabcontent);
        tabcontent.setVisibility(View.GONE);

        TabHost tabHost = getTabHost();

        TabHost.TabSpec tabSpec;

        tabSpec = tabHost.newTabSpec("tag1");
        tabSpec.setIndicator("", getResources().getDrawable(R.drawable.style_tabs_button_bp));
        tabSpec.setContent(new Intent(this, activity_main_game_backpack.class));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setIndicator("", getResources().getDrawable(R.drawable.style_tabs_button_sb));
        tabSpec.setContent(new Intent(this, activity_main_game_spell.class));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag3");
        tabSpec.setIndicator("", getResources().getDrawable(R.drawable.style_tabs_button_cb));
        tabSpec.setContent(new Intent(this, activity_main_game_cube.class));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag4");
        tabSpec.setIndicator("", getResources().getDrawable(R.drawable.style_tabs_button_mp));
        tabSpec.setContent(new Intent(this, activity_main_game_map.class));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag5");
        tabSpec.setIndicator("", getResources().getDrawable(R.drawable.style_tabs_button_pr));
        tabSpec.setContent(new Intent(this, activity_main_game_char.class));
        tabHost.addTab(tabSpec);


        final TabHost tabhost = findViewById(android.R.id.tabhost);

        getTabWidget().getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrolview1.setVisibility(View.GONE);
                tabcontent.setVisibility(View.VISIBLE);// custom code
                tabhost.setCurrentTab(0);
            }
        });

        getTabWidget().getChildAt(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrolview1.setVisibility(View.GONE);
                tabcontent.setVisibility(View.VISIBLE);// custom code
                tabhost.setCurrentTab(1);
            }
        });

        getTabWidget().getChildAt(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrolview1.setVisibility(View.GONE);
                tabcontent.setVisibility(View.VISIBLE);// custom code
                tabhost.setCurrentTab(2);
            }
        });

        getTabWidget().getChildAt(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrolview1.setVisibility(View.GONE);
                tabcontent.setVisibility(View.VISIBLE);// custom code
                tabhost.setCurrentTab(3);
            }
        });

        getTabWidget().getChildAt(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrolview1.setVisibility(View.GONE);
                tabcontent.setVisibility(View.VISIBLE);// custom code
                tabhost.setCurrentTab(4);
            }
        });


        fonts_load();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(activity_main_game.this, MainActivity.class);
        save_lvl();
        save_attack();
        save_HGAD();
        startActivity(intent);
    }

    public void load_avatar_one(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, IMG_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {

                Uri URI = data.getData();
                String[] FILE = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(URI, FILE, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(FILE[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();

                imageAvatar.setImageBitmap(BitmapFactory.decodeFile(ImageDecode));

                SP_avatar = getSharedPreferences("MyPref_avatar", MODE_PRIVATE);
                SharedPreferences.Editor ed_avatar = SP_avatar.edit();
                ed_avatar.putString(SAVED_TEXT1, ImageDecode);
                ed_avatar.apply();

            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }

    }

    public void fonts_load(){

        Typeface roboto_light = Typeface.createFromAsset(getAssets(),
                "Roboto_Light.ttf"); //use this.getAssets if you are calling from an Activity
        txtInitiative.setTypeface(roboto_light);
        txtArmClass.setTypeface(roboto_light);
        txtLvl.setTypeface(roboto_light);
        txtRun1.setTypeface(roboto_light);
        txtMelee.setTypeface(roboto_light);
        txtRange.setTypeface(roboto_light);
        txtSpell.setTypeface(roboto_light);

        txtEditWisdom.setTypeface(roboto_light);
        txtEditCharisma.setTypeface(roboto_light);
        txtEditMind.setTypeface(roboto_light);
        txtEditAgility.setTypeface(roboto_light);
        txtEditPhysique.setTypeface(roboto_light);
        txtEditForce.setTypeface(roboto_light);

        Typeface roboto_bold = Typeface.createFromAsset(getAssets(),
                "Roboto_Bold.ttf"); //use this.getAssets if you are calling from an Activity
        txtEditName.setTypeface(roboto_bold);

        Typeface roboto_thin = Typeface.createFromAsset(getAssets(),
                "Roboto_Thin.ttf"); //use this.getAssets if you are calling from an Activity
        editAboutBlock.setTypeface(roboto_thin);

    }

    public void load_avatar(){

        SP_avatar = getSharedPreferences("MyPref_avatar", MODE_PRIVATE);
        String savedSP_avatar = SP_avatar.getString(SAVED_TEXT1, "0");

        if (!savedSP_avatar.equals("0"))
        { imageAvatar.setImageBitmap(BitmapFactory.decodeFile(savedSP_avatar));}

    }

    public void go_stats(View view) {
        //txtEditLvl
        Intent intent = new Intent(activity_main_game.this, activity_main_game_stats.class);
        intent.putExtra("lvl", txtEditLvl.getText().toString());
        startActivity(intent);
    }

    public void lvl_up(View view) {
        final Dialog dialog_lvl = new Dialog(context);
        dialog_lvl.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_lvl.setContentView(R.layout.custom_alert_lvl);
        Button dialogButton = dialog_lvl.findViewById(R.id.but_ok_lvl);

        Button but_lvl_min = dialog_lvl.findViewById(R.id.but_lvl_min);
        Button but_lvl_max = dialog_lvl.findViewById(R.id.but_lvl_max);
        TextView lvl_char = dialog_lvl.findViewById(R.id.lvl_char);

        lvl_char.setText(txtEditLvl.getText().toString());

        but_lvl_min.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                TextView lvl_char = dialog_lvl.findViewById(R.id.lvl_char);
                CharSequence zz = lvl_char.getText();
                int pz= Integer.valueOf(zz.toString());
                pz=pz-1;
                lvl_char.setText(Integer.toString(pz));
            }
        });

        but_lvl_max.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                TextView lvl_char = dialog_lvl.findViewById(R.id.lvl_char);
                CharSequence zz = lvl_char.getText();
                int pz= Integer.valueOf(zz.toString());
                pz=pz+1;
                lvl_char.setText(Integer.toString(pz));
            }
        });

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_lvl.dismiss();
                TextView lvl_char = dialog_lvl.findViewById(R.id.lvl_char);
                txtEditLvl.setText(lvl_char.getText().toString());

            }
        });
        dialog_lvl.show();
        Button but_cancel_al = dialog_lvl.findViewById(R.id.but_cancel_al);
        but_cancel_al.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_lvl.dismiss();
                //Toast.makeText(getApplicationContext(),"Close..!!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void change_hp(View view) {
        final Dialog dialog_hp = new Dialog(context);
        dialog_hp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_hp.setContentView(R.layout.custom_alert_hp);
        Button dialogButton = dialog_hp.findViewById(R.id.but_ok_hp);

        Button but_hp_min = dialog_hp.findViewById(R.id.but_hp_min);
        Button but_hp_max = dialog_hp.findViewById(R.id.but_hp_max);
        TextView hp_char = dialog_hp.findViewById(R.id.hp_char); //change
        TextView hp_max = dialog_hp.findViewById(R.id.hp_max); //const

        hp_char.setText(textView16.getText().toString());

        //function  load save max hp in set text hp_max
        sPref5_healt = getSharedPreferences("MyPref5_healt", MODE_PRIVATE);
        String savedSP_healt = sPref5_healt.getString(SAVED_TEXT1, "0");
        hp_max.setText(savedSP_healt);

        but_hp_min.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                TextView hp_max = dialog_hp.findViewById(R.id.hp_max);
                TextView hp_char = dialog_hp.findViewById(R.id.hp_char);
                if(!hp_max.getText().toString().equals("")) {
                    CharSequence zz = hp_char.getText();
                    int pz = Integer.valueOf(zz.toString());
                    pz = pz - 1;
                    if (pz < 0)
                        hp_char.setText("0");
                    else
                        hp_char.setText(Integer.toString(pz));
                }
            }
        });

        but_hp_max.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                TextView hp_max = dialog_hp.findViewById(R.id.hp_max);
                TextView hp_char = dialog_hp.findViewById(R.id.hp_char);
                if(!hp_max.getText().toString().equals("")) {
                    CharSequence zz = hp_char.getText();
                    CharSequence zz_max = hp_max.getText();
                    int pz = Integer.valueOf(zz.toString());
                    int pz_max = Integer.valueOf(zz_max.toString());
                    pz = pz + 1;
                    if (pz > pz_max)
                        hp_char.setText(zz_max);
                    else
                        hp_char.setText(Integer.toString(pz));
                }
            }
        });

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_hp.dismiss();
                TextView hp_char = dialog_hp.findViewById(R.id.hp_char);
                TextView hp_max = dialog_hp.findViewById(R.id.hp_max);
                textView16.setText(hp_char.getText().toString());

                sPref5_healt = getSharedPreferences("MyPref5_healt", MODE_PRIVATE);
                SharedPreferences.Editor ed5 = sPref5_healt.edit();
                ed5.putString(SAVED_TEXT1, hp_max.getText().toString());
                ed5.apply();

            }
        });
        dialog_hp.show();
        Button but_cancel_al = dialog_hp.findViewById(R.id.but_cancel_hp);
        but_cancel_al.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_hp.dismiss();
                //Toast.makeText(getApplicationContext(),"Close..!!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void load_variable_stats(){

        //initiative
        txtEditInitiative = findViewById(R.id.txtEditInitiative);
        txtEditInitiative.setText(txtEditAgility.getText().toString());
        //Arm Class
        txtEditArmClass = findViewById(R.id.txtEditArmClass);
        textView20 = findViewById(R.id.textView20);
        CharSequence zz = txtEditAgility.getText();
        CharSequence zz2 = textView20.getText();
        //if(!zz2.equals(null)) {
            int agility= Integer.valueOf(zz.toString());
            int armore= Integer.valueOf(zz2.toString());
            int arm_class = agility + armore;
            txtEditArmClass.setText(Integer.toString(arm_class));
        //}
    }

    public void load_save_stat(){

        sPref1_name = getSharedPreferences("MyPref1_name", MODE_PRIVATE);
        String savedSP_name = sPref1_name.getString(SAVED_TEXT1, "0");
        txtEditName.setText(savedSP_name);

        sPref5_healt_change = getSharedPreferences("MyPref5_healt_change", MODE_PRIVATE);
        String savedSP_healt = sPref5_healt_change.getString(SAVED_TEXT1, "0");
        textView16.setText(savedSP_healt);

        sPref6_run = getSharedPreferences("MyPref6_run", MODE_PRIVATE);
        String savedSP_run = sPref6_run.getString(SAVED_TEXT1, "0");
        txtEditRun.setText(savedSP_run);

    }

    public void save_lvl(){
        //txtEditLvl
        sPref_lvl = getSharedPreferences("MyPref_level", MODE_PRIVATE);
        SharedPreferences.Editor edlvl = sPref_lvl.edit();
        edlvl.putString(SAVED_TEXT1, txtEditLvl.getText().toString());
        edlvl.apply();
    }
    public void load_lvl(){

        sPref_lvl = getSharedPreferences("MyPref_level", MODE_PRIVATE);
        String savedSP_lvl = sPref_lvl.getString(SAVED_TEXT1, "1");
        txtEditLvl.setText(savedSP_lvl);
    }

    public void save_attack(){
        sPref_melee = getSharedPreferences("MyPre_melee", MODE_PRIVATE);
        SharedPreferences.Editor ed_melee = sPref_melee.edit();
        ed_melee.putString(SAVED_TEXT1, txtEditMelee.getText().toString());
        ed_melee.apply();

        sPref_range = getSharedPreferences("MyPre_range", MODE_PRIVATE);
        SharedPreferences.Editor ed_range = sPref_range.edit();
        ed_range.putString(SAVED_TEXT1, txtEditRange.getText().toString());
        ed_range.apply();

        sPref_spell = getSharedPreferences("MyPre_spell", MODE_PRIVATE);
        SharedPreferences.Editor ed_spell = sPref_spell.edit();
        ed_spell.putString(SAVED_TEXT1, txtEditSpell.getText().toString());
        ed_spell.apply();
    }
    public void load_attack(){
        sPref_melee = getSharedPreferences("MyPre_melee", MODE_PRIVATE);
        String savedSP_melee = sPref_melee.getString(SAVED_TEXT1, "0");
        txtEditMelee.setText(savedSP_melee);

        sPref_range = getSharedPreferences("MyPre_range", MODE_PRIVATE);
        String savedSP_range = sPref_range.getString(SAVED_TEXT1, "0");
        txtEditRange.setText(savedSP_range);

        sPref_spell = getSharedPreferences("MyPre_spell", MODE_PRIVATE);
        String savedSP_spell = sPref_spell.getString(SAVED_TEXT1, "0");
        txtEditSpell.setText(savedSP_spell);
    }

    public void save_HGAD(){
        sPref5_healt_change = getSharedPreferences("MyPref5_healt_change", MODE_PRIVATE);
        SharedPreferences.Editor ed5 = sPref5_healt_change.edit();
        ed5.putString(SAVED_TEXT1, textView16.getText().toString());
        ed5.apply();

        sPref_gold = getSharedPreferences("MyPref_gold", MODE_PRIVATE);
        SharedPreferences.Editor ed_gold = sPref_gold.edit();
        ed_gold.putString(SAVED_TEXT1, textView18.getText().toString());
        ed_gold.apply();

        sPref_armor = getSharedPreferences("MyPref_armor", MODE_PRIVATE);
        SharedPreferences.Editor ed_armor = sPref_armor.edit();
        ed_armor.putString(SAVED_TEXT1, textView20.getText().toString());
        ed_armor.apply();

        sPref_damage = getSharedPreferences("MyPref_damage", MODE_PRIVATE);
        SharedPreferences.Editor ed_damage = sPref_damage.edit();
        ed_damage.putString(SAVED_TEXT1, textView23.getText().toString());
        ed_damage.apply();

        sPref_note = getSharedPreferences("MyPref_note", MODE_PRIVATE);
        SharedPreferences.Editor ed_note = sPref_note.edit();
        ed_note.putString(SAVED_TEXT1, editAboutBlock.getText().toString());
        ed_note.apply();
    }
    public void load_HGAD(){
        sPref_gold = getSharedPreferences("MyPref_gold", MODE_PRIVATE);
        String savedSP_gold = sPref_gold.getString(SAVED_TEXT1, "0");
        textView18.setText(savedSP_gold);

        sPref_armor = getSharedPreferences("MyPref_armor", MODE_PRIVATE);
        String savedSP_armor = sPref_armor.getString(SAVED_TEXT1, "0");
        textView20.setText(savedSP_armor);

        sPref_damage = getSharedPreferences("MyPref_damage", MODE_PRIVATE);
        String savedSP_damage = sPref_damage.getString(SAVED_TEXT1, "0");
        textView23.setText(savedSP_damage);

        sPref_note = getSharedPreferences("MyPref_note", MODE_PRIVATE);
        String savedSP_note = sPref_note.getString(SAVED_TEXT1, "");
        editAboutBlock.setText(savedSP_note);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        save_lvl();
        save_attack();
        save_HGAD();
    }

    @Override
    public void onPause() {
        super.onPause();
        save_lvl();
        save_attack();
        save_HGAD();
    }

}
