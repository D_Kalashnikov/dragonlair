package com.example.death.dragonlair2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class activity_new extends AppCompatActivity {

    Spinner spinner;
    Button button_continue;
    TextView txtName, txtRace, txtClass, txtWorldview, txtHealt, txtRun, txtAbout;
    EditText editName, editRace, editClass, editHealt, editRun, editAbout;

    SharedPreferences sPref1_name, sPref2_race, sPref3_class, sPref4_worldview, sPref5_healt, sPref6_run, sPref7_about, sPref1_temp, sPref1_temp_dat;
    final String SAVED_TEXT1 = "saved_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        spinner = findViewById(R.id.spinner);

        /*ArrayAdapter<CharSequence> dadapter = ArrayAdapter.createFromResource(this, R.array.Worldview, R.layout.row);
        dadapter.setDropDownViewResource(R.layout.row2);
        spinner.setAdapter(dadapter);*/

        button_continue = findViewById(R.id.button_continue);

        txtName = findViewById(R.id.txtName);
        txtRace = findViewById(R.id.txtRace);
        txtClass = findViewById(R.id.txtClass);
        txtWorldview = findViewById(R.id.txtWorldview);
        txtHealt = findViewById(R.id.txtHealt);
        txtRun = findViewById(R.id.txtRun);
        txtAbout = findViewById(R.id.txtAbout);

        editName = findViewById(R.id.editName);
        editRace = findViewById(R.id.editRace);
        editClass = findViewById(R.id.editClass);
        editHealt = findViewById(R.id.editHealt);
        editRun = findViewById(R.id.editRun);
        editAbout = findViewById(R.id.editAbout);


        Typeface roboto = Typeface.createFromAsset(getAssets(),
                "Roboto_Regular.ttf"); //use this.getAssets if you are calling from an Activity
        button_continue.setTypeface(roboto);

        editName.setTypeface(roboto);
        editRace.setTypeface(roboto);
        editClass.setTypeface(roboto);
        editHealt.setTypeface(roboto);
        editRun.setTypeface(roboto);


        Typeface roboto_light = Typeface.createFromAsset(getAssets(),
                "Roboto_Light.ttf"); //use this.getAssets if you are calling from an Activity
        txtName.setTypeface(roboto_light);
        txtRace.setTypeface(roboto_light);
        txtClass.setTypeface(roboto_light);
        txtWorldview.setTypeface(roboto_light);
        txtHealt.setTypeface(roboto_light);
        txtRun.setTypeface(roboto_light);
        txtAbout.setTypeface(roboto_light);

        editAbout.setTypeface(roboto_light);



    }

    public void next(View view) {
        String hp = editHealt.getText().toString();
        String run = editRun.getText().toString();
        if(!hp.equals("") && !run.equals("")) {
            Intent intent = new Intent(activity_new.this, activity_main_game.class);
            intent.putExtra("new_name", editName.getText().toString());
            intent.putExtra("new_race", editRace.getText().toString());
            intent.putExtra("new_class", editClass.getText().toString());
            intent.putExtra("new_healt", editHealt.getText().toString());
            intent.putExtra("new_run", editRun.getText().toString());
            intent.putExtra("new_about", editAbout.getText().toString());
            save();

            delete_save();

            startActivity(intent);
        }
        else Toast.makeText(this, "Заполните поля", Toast.LENGTH_SHORT).show();
    }

    public void save(){

        sPref1_name = getSharedPreferences("MyPref1_name", MODE_PRIVATE);
        SharedPreferences.Editor ed1 = sPref1_name.edit();
        ed1.putString(SAVED_TEXT1, editName.getText().toString());
        ed1.apply();

        sPref2_race = getSharedPreferences("MyPref2_race", MODE_PRIVATE);
        SharedPreferences.Editor ed2 = sPref2_race.edit();
        ed2.putString(SAVED_TEXT1, editRace.getText().toString());
        ed2.apply();

        sPref3_class = getSharedPreferences("MyPref3_class", MODE_PRIVATE);
        SharedPreferences.Editor ed3 = sPref3_class.edit();
        ed3.putString(SAVED_TEXT1, editClass.getText().toString());
        ed3.apply();

        sPref4_worldview = getSharedPreferences("MyPref4_worldview", MODE_PRIVATE);
        SharedPreferences.Editor ed4 = sPref4_worldview.edit();
        ed4.putString(SAVED_TEXT1, Integer.toString(spinner.getSelectedItemPosition()));
        ed4.apply();

        sPref5_healt = getSharedPreferences("MyPref5_healt", MODE_PRIVATE);
        SharedPreferences.Editor ed5 = sPref5_healt.edit();
        ed5.putString(SAVED_TEXT1, editHealt.getText().toString());
        ed5.apply();

        sPref6_run = getSharedPreferences("MyPref6_run", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref6_run.edit();
        ed.putString(SAVED_TEXT1, editRun.getText().toString());
        ed.apply();

        sPref7_about = getSharedPreferences("MyPref7_about", MODE_PRIVATE);
        SharedPreferences.Editor ed7 = sPref7_about.edit();
        ed7.putString(SAVED_TEXT1, editAbout.getText().toString());
        ed7.apply();

        //param logic start
        sPref1_temp = getSharedPreferences("MyPrefA_temp", MODE_PRIVATE);
        SharedPreferences.Editor ed_temp = sPref1_temp.edit();
        ed_temp.putString(SAVED_TEXT1, "1" );
        ed_temp.apply();

        sPref1_temp_dat = getSharedPreferences("MyPrefA_temp_dat", MODE_PRIVATE);
        SharedPreferences.Editor ed_temp_dat = sPref1_temp_dat.edit();
        ed_temp_dat.putString(SAVED_TEXT1, "1" );
        ed_temp_dat.apply();

    }

    public void delete_save(){

        SharedPreferences settings = this.getSharedPreferences("MyPref_level", Context.MODE_PRIVATE);
        settings.edit().clear().apply();

        SharedPreferences settings1 = this.getSharedPreferences("MyPre_melee", Context.MODE_PRIVATE);
        settings1.edit().clear().apply();

        SharedPreferences settings2 = this.getSharedPreferences("MyPre_range", Context.MODE_PRIVATE);
        settings2.edit().clear().apply();

        SharedPreferences settings3 = this.getSharedPreferences("MyPre_spell", Context.MODE_PRIVATE);
        settings3.edit().clear().apply();

        SharedPreferences settings4 = this.getSharedPreferences("MyPref_gold", Context.MODE_PRIVATE);
        settings4.edit().clear().apply();

        SharedPreferences settings5 = this.getSharedPreferences("MyPref_damage", Context.MODE_PRIVATE);
        settings5.edit().clear().apply();

        SharedPreferences settings6 = this.getSharedPreferences("MyPref_note", Context.MODE_PRIVATE);
        settings6.edit().clear().apply();

        SharedPreferences settings7 = this.getSharedPreferences("MyPrefb_weapon", Context.MODE_PRIVATE);
        settings7.edit().clear().apply();

        SharedPreferences settings8 = this.getSharedPreferences("MyPrefb_armore", Context.MODE_PRIVATE);
        settings8.edit().clear().apply();

        SharedPreferences settings9 = this.getSharedPreferences("MyPrefb_accessor", Context.MODE_PRIVATE);
        settings9.edit().clear().apply();

        SharedPreferences settings10 = this.getSharedPreferences("MyPrefb_potions", Context.MODE_PRIVATE);
        settings10.edit().clear().apply();

        SharedPreferences settings11 = this.getSharedPreferences("MyPrefb_etc", Context.MODE_PRIVATE);
        settings11.edit().clear().apply();

        SharedPreferences settings12 = this.getSharedPreferences("MyPref_armor", Context.MODE_PRIVATE);
        settings12.edit().clear().apply();

        SharedPreferences settings13 = this.getSharedPreferences("bun1", Context.MODE_PRIVATE);
        settings13.edit().clear().apply();

        SharedPreferences settings14 = this.getSharedPreferences("bun2", Context.MODE_PRIVATE);
        settings14.edit().clear().apply();

        SharedPreferences settings15 = this.getSharedPreferences("bun3", Context.MODE_PRIVATE);
        settings15.edit().clear().apply();

        SharedPreferences settings16 = this.getSharedPreferences("bun4", Context.MODE_PRIVATE);
        settings16.edit().clear().apply();

        SharedPreferences settings17 = this.getSharedPreferences("bun5", Context.MODE_PRIVATE);
        settings17.edit().clear().apply();

        SharedPreferences settings18 = this.getSharedPreferences("bun6", Context.MODE_PRIVATE);
        settings18.edit().clear().apply();

        SharedPreferences settings19 = this.getSharedPreferences("bun8", Context.MODE_PRIVATE);
        settings19.edit().clear().apply();

        SharedPreferences settings20 = this.getSharedPreferences("bun9", Context.MODE_PRIVATE);
        settings20.edit().clear().apply();

        SharedPreferences settings21 = this.getSharedPreferences("bun10", Context.MODE_PRIVATE);
        settings21.edit().clear().apply();

        SharedPreferences settings22 = this.getSharedPreferences("bun11", Context.MODE_PRIVATE);
        settings22.edit().clear().apply();

        SharedPreferences settings23 = this.getSharedPreferences("bun12", Context.MODE_PRIVATE);
        settings23.edit().clear().apply();

        SharedPreferences settings24 = this.getSharedPreferences("bun13", Context.MODE_PRIVATE);
        settings24.edit().clear().apply();

        SharedPreferences settings25 = this.getSharedPreferences("bun14", Context.MODE_PRIVATE);
        settings25.edit().clear().apply();

        SharedPreferences settings26 = this.getSharedPreferences("bun15", Context.MODE_PRIVATE);
        settings26.edit().clear().apply();

        SharedPreferences settings27 = this.getSharedPreferences("bun16", Context.MODE_PRIVATE);
        settings27.edit().clear().apply();

        SharedPreferences settings28 = this.getSharedPreferences("bun17", Context.MODE_PRIVATE);
        settings28.edit().clear().apply();

        SharedPreferences settings29 = this.getSharedPreferences("bun18", Context.MODE_PRIVATE);
        settings29.edit().clear().apply();

        SharedPreferences settings30 = this.getSharedPreferences("bun19", Context.MODE_PRIVATE);
        settings30.edit().clear().apply();

        SharedPreferences settings31 = this.getSharedPreferences("bun20", Context.MODE_PRIVATE);
        settings31.edit().clear().apply();

        SharedPreferences settings32 = this.getSharedPreferences("bun21", Context.MODE_PRIVATE);
        settings32.edit().clear().apply();

        SharedPreferences settings33 = this.getSharedPreferences("bun22", Context.MODE_PRIVATE);
        settings33.edit().clear().apply();

        SharedPreferences settings34 = this.getSharedPreferences("bun23", Context.MODE_PRIVATE);
        settings34.edit().clear().apply();

        SharedPreferences settings35 = this.getSharedPreferences("bun24", Context.MODE_PRIVATE);
        settings35.edit().clear().apply();

        SharedPreferences settings36 = this.getSharedPreferences("bun25", Context.MODE_PRIVATE);
        settings36.edit().clear().apply();

        SharedPreferences settings37 = this.getSharedPreferences("bun26", Context.MODE_PRIVATE);
        settings37.edit().clear().apply();

        SharedPreferences settings38 = this.getSharedPreferences("bun27", Context.MODE_PRIVATE);
        settings38.edit().clear().apply();

        SharedPreferences settings39 = this.getSharedPreferences("bun28", Context.MODE_PRIVATE);
        settings39.edit().clear().apply();

        SharedPreferences settings40 = this.getSharedPreferences("bun29", Context.MODE_PRIVATE);
        settings40.edit().clear().apply();

        SharedPreferences settings41 = this.getSharedPreferences("bun30", Context.MODE_PRIVATE);
        settings41.edit().clear().apply();

        SharedPreferences settings42 = this.getSharedPreferences("bun31", Context.MODE_PRIVATE);
        settings42.edit().clear().apply();

        SharedPreferences settings43 = this.getSharedPreferences("bunCheck", Context.MODE_PRIVATE);
        settings43.edit().clear().apply();

        SharedPreferences settings44 = this.getSharedPreferences("bunCheck2", Context.MODE_PRIVATE);
        settings44.edit().clear().apply();

        SharedPreferences settings45 = this.getSharedPreferences("bunCheck3", Context.MODE_PRIVATE);
        settings45.edit().clear().apply();

        SharedPreferences settings46 = this.getSharedPreferences("bunCheck4", Context.MODE_PRIVATE);
        settings46.edit().clear().apply();

        SharedPreferences settings47 = this.getSharedPreferences("bunCheck5", Context.MODE_PRIVATE);
        settings47.edit().clear().apply();

        SharedPreferences settings48 = this.getSharedPreferences("bunCheck6", Context.MODE_PRIVATE);
        settings48.edit().clear().apply();

        SharedPreferences settings49 = this.getSharedPreferences("bunCheck7", Context.MODE_PRIVATE);
        settings49.edit().clear().apply();

        SharedPreferences settings50 = this.getSharedPreferences("bunCheck8", Context.MODE_PRIVATE);
        settings50.edit().clear().apply();

        SharedPreferences settings51 = this.getSharedPreferences("bunCheck10", Context.MODE_PRIVATE);
        settings51.edit().clear().apply();

        SharedPreferences settings52 = this.getSharedPreferences("bunCheck11", Context.MODE_PRIVATE);
        settings52.edit().clear().apply();

        SharedPreferences settings53 = this.getSharedPreferences("bunCheck12", Context.MODE_PRIVATE);
        settings53.edit().clear().apply();

        SharedPreferences settings54 = this.getSharedPreferences("bunCheck13", Context.MODE_PRIVATE);
        settings54.edit().clear().apply();

        SharedPreferences settings55 = this.getSharedPreferences("bunCheck14", Context.MODE_PRIVATE);
        settings55.edit().clear().apply();

        SharedPreferences settings56 = this.getSharedPreferences("bunCheck15", Context.MODE_PRIVATE);
        settings56.edit().clear().apply();

        SharedPreferences settings57 = this.getSharedPreferences("bunCheck16", Context.MODE_PRIVATE);
        settings57.edit().clear().apply();

        SharedPreferences settings58 = this.getSharedPreferences("bunCheck17", Context.MODE_PRIVATE);
        settings58.edit().clear().apply();

        SharedPreferences settings59 = this.getSharedPreferences("bunCheck18", Context.MODE_PRIVATE);
        settings59.edit().clear().apply();

        SharedPreferences settings60 = this.getSharedPreferences("bunCheck19", Context.MODE_PRIVATE);
        settings60.edit().clear().apply();

        SharedPreferences settings61 = this.getSharedPreferences("MyPref5_healt_change", Context.MODE_PRIVATE);
        settings61.edit().clear().apply();

        SharedPreferences settings62 = this.getSharedPreferences("MyPref_avatar", Context.MODE_PRIVATE);
        settings62.edit().clear().apply();

        SharedPreferences settings63 = this.getSharedPreferences("MyPref_map", Context.MODE_PRIVATE);
        settings63.edit().clear().apply();

    }

    public void cls_ab(View view) {
        editAbout.setText("");
    }
}
