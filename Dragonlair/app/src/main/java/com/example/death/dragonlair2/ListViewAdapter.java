package com.example.death.dragonlair2;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Friend> {

    private activity_main_game_spell activity;
    DatabaseHelper databaseHelper;
    private List<Friend> friendList;


    public ListViewAdapter(activity_main_game_spell context, int resource, List<Friend> objects, DatabaseHelper helper) {
        super(context, resource, objects);
        this.activity = context;
        this.databaseHelper = helper;
        this.friendList = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(getItem(position).getName()); // возможно вывод имени в листвью
        holder.item_descr.setText(getItem(position).getJob());

        //Delete an item
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseHelper.deleteFriend(getItem(position)); //delete in db
                Toast.makeText(activity, "Удалено!", Toast.LENGTH_SHORT).show();
                //reload the database to view
                activity.reloadingDatabase();
            }
        });

        //Edit/Update an item
        holder.name.setOnEditorActionListener( new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if( event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                    // обработка нажатия Enter

                    Friend friend = new Friend(holder.name.getText().toString(), holder.item_descr.getText().toString());
                    friend.setId(getItem(position).getId());
                    databaseHelper.updateFriend(friend); //update to db
                    Toast.makeText(activity, "Обновлено!", Toast.LENGTH_SHORT).show();

                    //reload the database to view
                    activity.reloadingDatabase();

                    return true;
                }
                return false;
            }
        });

        holder.item_descr.setOnEditorActionListener( new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if( event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                    // обработка нажатия Enter

                    Friend friend = new Friend(holder.name.getText().toString(), holder.item_descr.getText().toString());
                    friend.setId(getItem(position).getId());
                    databaseHelper.updateFriend(friend); //update to db
                    Toast.makeText(activity, "Обновлено!", Toast.LENGTH_SHORT).show();

                    //reload the database to view
                    activity.reloadingDatabase();

                    return true;
                }
                return false;
            }
        });

            Typeface roboto_light = Typeface.createFromAsset(activity.getAssets(),
                "Roboto_Light.ttf"); //use this.getAssets if you are calling from an Activity
        holder.item_descr.setTypeface(roboto_light);

        return convertView;
    }

    private static class ViewHolder {
        private EditText name;
        private EditText item_descr;
        private View btnDelete;

        public ViewHolder (View v) {
            name = v.findViewById(R.id.item_name);
            item_descr = v.findViewById(R.id.item_descr);

            btnDelete = v.findViewById(R.id.delete);
        }
    }

}